import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name:'我的数据',
    age:0
  },
  mutations: {
    info(state,payload){
      state.age+=payload

      console.log(state)
    }
  },
  actions: {
    ascInfo(context,payload){
      setTimeout(()=>{
        console.log('异步提交+++',context)
        // commit('info',payload)
        context.state.age+=payload
      },1000)      
    }
  },
  modules: {
  }
})
