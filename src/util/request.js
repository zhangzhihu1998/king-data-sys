import axios from 'axios'

const serviceKing = axios.create({
	baseURL: '/king-api',
	timeout: 5000
})

// 请求拦截器
serviceKing.interceptors.request.use(
	config => {
		// config.headers.token = "222"
		console.log(config)
		return config
	}, error => {
		Promise.reject(error)
	}

	
)

// 相应拦截器
serviceKing.interceptors.response.use(
	res => {
		console.log(res)
		return res
	}
)




export { serviceKing }

