import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '../common.css'
import comDrag from "@/components/comDrag"

// Vue.component()   //挂载全局组件方法
// Vue.prototype.$axios = axios    //挂载全局的方法

Vue.config.productionTip = false
Vue.component('comDrag',comDrag)
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
