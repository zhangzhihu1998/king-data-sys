/**
 * Created by hustcc on 18/6/23.
 * Contract: i@hust.cc
 */

import CanvasNest from './CanvasNest';

const getScriptConfig = () => {
  const scripts = document.getElementsByTagName('script');
  const len = scripts.length;
  const script = scripts[len - 1]; // 当前加载的script
  return {
    zIndex: script.getAttribute('zIndex'),
    opacity: script.getAttribute('opacity'),
    color: script.getAttribute('color'),
    pointColor: script.getAttribute('pointColor'),
    count: Number(script.getAttribute('count')) || 99,
  };
  // color: 线条颜色，默认值：'0,0,0'; RGB 值：(R, G, B)。（注意：使用 ',' 分隔。）
  // pointColor: 点的颜色, 默认: '0,0,0'; RGB 值：(R, G, B)。（注意：使用 ',' 分隔。）
  // opacity: 线的不透明度(0~1), 默认: 0.5.
  //   count: 行数，默认：99.
  // zIndex: 背景的 z - index 属性，默认值：-1.
};

new CanvasNest(document.body, getScriptConfig());
