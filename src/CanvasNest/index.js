/**
 * Created by hustcc on 18/6/23.
 * Contract: i@hust.cc
 */
//  color: 线条颜色, 默认: '0,0,0' ；三个数字分别为(R,G,B)，注意用,分割
//  pointColor: 交点颜色, 默认: '0,0,0' ；三个数字分别为(R,G,B)，注意用,分割
//  opacity: 线条透明度（0~1）, 默认: 0.5
//  count: 线条的总数量, 默认: 150
//  zIndex: 背景的z-index属性，css属性用于控制所在层的位置, 默认: -1
import CanvasNest from './CanvasNest';

export default CanvasNest;
