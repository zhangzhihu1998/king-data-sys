import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/login"
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import("../views/Home"),
    redirect: '/banner',
    children: [
      {
        path: '/banner',
        name: 'banner',
        component: () => import("../views/banner/index.vue")
      },
      {
        path: '/my',
        name: 'my',
        component: () => import("../views/my/index.vue")
      },
      {
        path: '/comment',
        name: 'Comment',
        component: () => import("../views/comment/index.vue"),
      },
      {
        path: '/gameData/detail',
        name: 'detail',
        component: () => import("../views/gameData/detail"),
        meta: {
          bread: [
            { name: "首页", url: '/index', icon: 'el-icon-s-home' },
            { name: '英雄数据', url: '/gameData/heroData', icon: 'el-icon-s-custom' },
            { name: '详细信息', url: '/gemaData/detail', icon: 'el-icon-location' },
          ]
        }
      },
      {
        path: '/gameData/heroData',
        name: 'heroData',
        component: () => import("../views/gameData/heroData"),
        meta: {
          bread: [
            { name: "首页", url: '/index' },
            { name: '英雄数据', url: '/gameData/heroData' },
          ]

        }
      },
      {
        path: '/gameData/equipData',
        name: 'equipData',
        component: () => import("../views/gameData/equipData"),
        meta: {
          bread: [
            { name: "首页", url: '/index' },
            { name: '装备数据', url: '/gameData/equipData' },
          ]
        }
      },
      {
        path: '/about',
        name: 'About',
        component: () => import('../views/about')
      },
      {
        path: '/test',
        name: 'test',
        component: () => import('../views/test.vue')
      }
    ]
  }
  // {
  // 	path: '/login',
  // 	name: 'login',
  // 	component: () => import("../views/login.vue")
  // },

]



const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, from, next) => {
// 	if (true) {
// 		if (to.path == '/login') {
// 			console.log(9897, to.path)
// 			// next(from.path)
// 		} else {
// 			next()
// 		}
// 	} else {
// 		if (to.path == '/login') {
// 			next()
// 		} else {
// 			next('/login')
// 		}
// 	}

// })


export default router
