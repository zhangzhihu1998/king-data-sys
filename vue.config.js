module.exports = {
	devServer: {
		proxy: {
			"/king-api": {
				target: "http://gamehelper.gm825.com",
				// target: "http://39.104.92.226:8080",
				changeOrigin: true,
				pathRewrite: {
					"^/king-api": ""
				}
			},
			"/bpi": {
				target: 'https://jsonplaceholder.typicode.com/posts/',
				changeOrigin: true,
				pathRewrite: {
					"^/bpi": ""
				}
			},
			"/cpi": {
				target: 'https://apps.game.qq.com/cmc/cross',
				changeOrigin: true,
				pathRewrite: {
					"^/cpi": ""
				}
			},

		}
	}
}